var items = [
    ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpg'], 
    ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpg'],
    ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
    ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpg']
]

let cartNum = 0;

let listBarang = document.getElementById('listBarang');
let formItem = document.getElementById('formItem');
let cart = document.getElementById('cart');
let icon = document.getElementById('icon');

listBarang.innerHTML = createView(items);
let addButtons = document.getElementsByClassName('add_cart');

formItem.addEventListener("submit", ()=>{
    let keywordInput = document.getElementById('keyword').value.toLowerCase();

    let newArray = searchItem(keywordInput, items);
    listBarang.innerHTML = createView(newArray);
});

for (const button of addButtons) {
    button.addEventListener('click', ()=>{
        cartNum ++;
        cart.innerHTML = '<i class="fas fa-shopping-cart" id="icon"></i>(' + cartNum + ')';
    });
}

cart.innerHTML = '<i class="fas fa-shopping-cart" id="icon"></i>(' + cartNum + ')';

function createView(products){
    let tag = ''
    for (const product of products) {
        tag += '<div class ="col-4 mt-5">'
        tag += `<div class="card" style="width: 18rem;" id="${product[0]}">`
        tag += `<img src="img/${product[4]}" class="card-img-top" style="object-fit:cover;" height="200px" alt="img/${product[1]}">`
        tag += '<div class="card-body">'
        tag += `<h5 class="card-title" id="itemName">${product[1]}</h5>`
        tag += `<p class="card-text" id="itemDesc">${product[3]}</p>`
        tag += `<p class="card-text">${product[2]}</p>
                </div>`
        tag += `<div class="card-footer">
                <a href="#" class="btn btn-primary add_cart" id="addCart">Tambahkan ke keranjang</a>
                </div>
                </div>
                </div>`
    }
    return tag;
}

function searchItem(keyword, productList){
    let filteredList = productList.filter(product => product[1].toLowerCase().includes(keyword))
    return filteredList;
}